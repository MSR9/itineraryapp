//
//  UIButtonExtension.swift
//  Itinerary
//
//  Created by Prahlad Reddy on 6/13/18.
//  Copyright © 2018 Prahlad Reddy. All rights reserved.
//

import UIKit

extension UIButton {
    func createFloatingActionButton() {
        backgroundColor = Theme.tintColor
        layer.cornerRadius = frame.height / 2
        layer.shadowOpacity = 0.25
        layer.shadowRadius = 5
        layer.shadowOffset = CGSize(width: 0, height: 10)
    }
}
