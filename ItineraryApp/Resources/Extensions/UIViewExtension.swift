//
//  UIViewExtensions.swift
//  Itinerary
//
//  Created by Prahlad Reddy on 5/25/18.
//  Copyright © 2018 Prahlad Reddy. All rights reserved.
//

import UIKit

extension UIView {
    func addShadowAndRoundedCorners() {
        layer.shadowOpacity = 1
        layer.shadowOffset = CGSize.zero
        layer.shadowColor = UIColor.darkGray.cgColor
        layer.cornerRadius = 10
    }
}
