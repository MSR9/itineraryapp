//
//  AddActivityViewController.swift
//  Itinerary
//
//  Created by Prahlad Reddy on 2/9/19.
//  Copyright © 2019 Prahlad Reddy. All rights reserved.
//

import UIKit

class AddActivityViewController: UITableViewController {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dayPickerView: UIPickerView!
    @IBOutlet weak var titleTextField: UITextField!
    @IBOutlet weak var subtitleTextField: UITextField!
    @IBOutlet var activityTypeButtons: [UIButton]!
    
    var doneSaving: ((Int, ActivityModel) -> ())?
    var tripIndex: Int! // Needed for saving
    var tripModel: TripModel! // Needed for showing days in picker view
    
    // For editing Activities
    var dayIndexToEdit: Int?
    var activityModelToEdit: ActivityModel!
    var doneUpdating: ((Int, Int, ActivityModel) -> ())?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        titleLabel.font = UIFont(name: Theme.mainFontName, size: 24)
        
        dayPickerView.dataSource = self
        dayPickerView.delegate = self
        
        
        if let dayIndex = dayIndexToEdit, let activityModel = activityModelToEdit {
            // Update Activity: Popuplate the popup
            titleLabel.text = "Edit Activity"
            
            // Select the Day in the Picker View
            dayPickerView.selectRow(dayIndex, inComponent: 0, animated: true)
            
            // Populate the Activity Data
            activityTypeSelected(activityTypeButtons[activityModel.activityType.rawValue])
            titleTextField.text = activityModel.title
            subtitleTextField.text = activityModel.subTitle
        } else {
            // New Activity: Set default values
            activityTypeSelected(activityTypeButtons[ActivityType.excursion.rawValue])
        }
    }
    
    @IBAction func activityTypeSelected(_ sender: UIButton) {
        activityTypeButtons.forEach({ $0.tintColor = Theme.accent })
        sender.tintColor = Theme.tintColor
    }
    
    @IBAction func save(_ sender: UIButton) {
        guard titleTextField.hasValue, let newTitle = titleTextField.text else { return }
        let activityType: ActivityType = getSelectedActivityType()
        
        let newDayIndex = dayPickerView.selectedRow(inComponent: 0)
        
        if activityModelToEdit != nil {
            // Update Activity
            activityModelToEdit.activityType = activityType
            activityModelToEdit.title = newTitle
            activityModelToEdit.subTitle = subtitleTextField.text ?? ""
            
            ActivityFunctions.updateActivity(at: tripIndex, oldDayIndex: dayIndexToEdit!, newDayIndex: newDayIndex, using: activityModelToEdit)
            
            if let doneUpdating = doneUpdating, let oldDayIndex = dayIndexToEdit {
                doneUpdating(oldDayIndex, newDayIndex, activityModelToEdit)
            }
        } else {
            // New Activity
            let activityModel = ActivityModel(title: newTitle, subTitle: subtitleTextField.text ?? "", activityType: activityType)
            ActivityFunctions.createActivity(at: tripIndex, for: newDayIndex, using: activityModel)
            
            if let doneSaving = doneSaving {
                doneSaving(newDayIndex, activityModel)
            }
        }
        
        dismiss(animated: true)
    }
    
    func getSelectedActivityType() -> ActivityType {
        for (index, button) in activityTypeButtons.enumerated() {
            if button.tintColor == Theme.tintColor {
                return ActivityType(rawValue: index) ?? .excursion
            }
        }
        
        return .excursion
    }
    
    @IBAction func cancel(_ sender: UIButton) {
        dismiss(animated: true)
    }
    
    @IBAction func done(_ sender: UITextField) {
        sender.resignFirstResponder()
    }
}

extension AddActivityViewController: UIPickerViewDataSource, UIPickerViewDelegate {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return tripModel.dayModels.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return tripModel.dayModels[row].title.mediumDate()
    }
}
