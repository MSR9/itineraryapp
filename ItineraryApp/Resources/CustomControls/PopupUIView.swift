//
//  PopupUIView.swift
//  Itinerary
//
//  Created by Prahlad Reddy on 7/16/18.
//  Copyright © 2018 Prahlad Reddy. All rights reserved.
//

import UIKit

class PopupUIView: UIView {

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        layer.shadowOpacity = 1
        layer.shadowOffset = CGSize.zero
        layer.shadowColor = UIColor.darkGray.cgColor
        layer.cornerRadius = 10
        
        backgroundColor = Theme.backgroundColor
    }
}
