//
//  ActivityTypeEnum.swift
//  Itinerary
//
//  Created by Prahlad Reddy on 11/19/18.
//  Copyright © 2018 Prahlad Reddy. All rights reserved.
//

import Foundation

enum ActivityType: Int {
    case auto
    case excursion
    case flight
    case food
    case hotel
}
