//
//  DayFunctions.swift
//  Itinerary
//
//  Created by Prahlad Reddy on 12/30/18.
//  Copyright © 2018 Prahlad Reddy. All rights reserved.
//

import Foundation

class DayFunctions {
    static func createDay(at tripIndex: Int, using dayModel: DayModel) {
        // Replace with real data store code
        
        Data.tripModels[tripIndex].dayModels.append(dayModel)
    }
}
