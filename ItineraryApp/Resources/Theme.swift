//
//  Theme.swift
//  Itinerary
//
//  Created by Prahlad Reddy on 6/9/18.
//  Copyright © 2018 Prahlad Reddy. All rights reserved.
//

import UIKit

class Theme {
    static let mainFontName = "FugazOne-Regular"
    static let bodyFontName = "AvenirNext-Regular"
    static let bodyFontNameBold = "AvenirNext-Bold"
    static let bodyFontNameDemiBold = "AvenirNext-DemiBold"
    static let accent = UIColor(named: "Accent")
    static let backgroundColor = UIColor(named: "Background")
    static let tintColor = UIColor(named: "Tint")
}
