//
//  TripModel.swift
//  Itinerary
//
//  Created by Prahlad Reddy on 4/29/18.
//  Copyright © 2018 Prahlad Reddy. All rights reserved.
//

import UIKit

struct TripModel {
    let id: UUID
    var title: String
    var image: UIImage?
    var dayModels = [DayModel]() {
        didSet {
            // Called when a new value is assigned to dayModels
//            dayModels = dayModels.sorted(by: { (dayModel1, dayModel2) -> Bool in
//                dayModel1.title < dayModel2.title
//            })
            
//            dayModels = dayModels.sorted(by: { $0.title < $1.title })
            
            dayModels = dayModels.sorted(by: <)
        }
    }
    
    init(title: String, image: UIImage? = nil, dayModels: [DayModel]? = nil) {
        id = UUID()
        self.title = title
        self.image = image
        
        if let dayModels = dayModels {
            self.dayModels = dayModels
        }
    }
}
