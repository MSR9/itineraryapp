//
//  UIViewControllerExtension.swift
//  Itinerary
//
//  Created by Prahlad Reddy on 12/30/18.
//  Copyright © 2018 Prahlad Reddy. All rights reserved.
//

import UIKit

extension UIViewController {
    
    /**
     Just returns the initial view controller on a storyboard
     */
    static func getInstance() -> UIViewController {
        let storyboard = UIStoryboard(name: String(describing: self), bundle: nil)
        return storyboard.instantiateInitialViewController()!
    }
}
