//
//  UITableViewCellExtensions.swift
//  Itinerary
//
//  Created by Prahlad Reddy on 12/9/18.
//  Copyright © 2018 Prahlad Reddy. All rights reserved.
//

import UIKit

extension UITableViewCell {
    /// Returns a string representation of this class
    class var identifier: String {
        return String(describing: self)
    }
}
