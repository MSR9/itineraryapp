//
//  UITextFieldExtension.swift
//  Itinerary
//
//  Created by Prahlad Reddy on 12/30/18.
//  Copyright © 2018 Prahlad Reddy. All rights reserved.
//

import UIKit

extension UITextField {
    var hasValue: Bool {
        guard text == "" else { return true }
        
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 30, height: 20))
        imageView.image = #imageLiteral(resourceName: "Warning")
        imageView.contentMode = .scaleAspectFit
        
        rightView = imageView
        rightViewMode = .unlessEditing
        
        return false
    }
}
