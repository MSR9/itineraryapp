//
//  HeaderTableViewCell.swift
//  Itinerary
//
//  Created by Prahlad Reddy on 12/2/18.
//  Copyright © 2018 Prahlad Reddy. All rights reserved.
//

import UIKit

class HeaderTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        titleLabel.font = UIFont(name: Theme.bodyFontNameBold, size: 17)
        subtitleLabel.font = UIFont(name: Theme.bodyFontName, size: 15)
    }

    func setup(model: DayModel) {
        titleLabel.text = model.title.mediumDate()
        subtitleLabel.text = model.subtitle
    }
}
