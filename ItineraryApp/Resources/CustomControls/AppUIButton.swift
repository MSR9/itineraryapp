//
//  AppUIButton.swift
//  Itinerary
//
//  Created by Prahlad Reddy on 7/16/18.
//  Copyright © 2018 Prahlad Reddy. All rights reserved.
//

import UIKit

class AppUIButton: UIButton {

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        backgroundColor = Theme.tintColor
        layer.cornerRadius = frame.height / 2
        setTitleColor(UIColor.white, for: .normal)
    }

}
